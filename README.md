# BridgeStatus

This git repo shows the vantage point report from different regions.

Following vantage points are active:
- cn: China VPS
- russia: Russia residential network
- iran: Iran VPS

For each result file, there is the following fields:

1. Test Target Name: if it does not start with snowflake, this name identifies the test case as input to the bridge_lines.txt file. if it starts with snowflake, then it is one of ten attempts to try snowflake.
2. Tor bootstrap percentage. 2: socks failure; 10: blocked, 50: proxy too slow, 100: working.
3. Test Date In human-readable format: The time when the last report is received. To tell the difference between proxy working, and both proxy and probe not working.
4. Test Date In Unix timestamp: The time when the last report is received, for scripting.
5. Keyed-Hashed Bridge Line: Also known as Bridge Reincarnation Event Identification Nonce. When a dynamic IP address S96 bridge changes its IP address, this value will change. This will be used to research how long does it take for a bridge to be blocked in different conditions.
6. the bootstrap time. It is the time Tor takes to finish downloading initial protocol data required to function, and can be used as a indicator of connection quality and user experience.
7. Download link for raw probe log, if available. This link is password protected and will expire.
